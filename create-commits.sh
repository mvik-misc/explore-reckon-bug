#!/usr/bin/env bash

touch file-with-changes.txt
git add file-with-changes.txt

echo "change #1" >> file-with-changes.txt
git commit -am "Add change #1"
echo "change #2" >> file-with-changes.txt
git commit -am "Add change #2"
echo "change #3" >> file-with-changes.txt
git commit -am "Add change #3"
echo "change #4" >> file-with-changes.txt
git commit -am "Add change #4"
echo "change #5" >> file-with-changes.txt
git commit -am "Add change #5"
echo "change #6" >> file-with-changes.txt
git commit -am "Add change #6"
echo "change #7" >> file-with-changes.txt
git commit -am "Add change #7"
echo "change #8" >> file-with-changes.txt
git commit -am "Add change #8"
echo "change #9" >> file-with-changes.txt
git commit -am "Add change #9"